/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath.weatherclient.resources;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)

/**
 *
 * @author Nick
 */
public class City {

    private String name;
    private Coord coord;
    private String country;

    public City() {
        
    }

    public String getName() {
        return name;
    }


    public Coord getCoord() {
        return coord;
    }

    public String getCountry() {
        return country;
    }
    
    
    
}
