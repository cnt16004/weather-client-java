/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath.weatherclient.resources;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)

/**
 *
 * @author Nick
 */
public class WeatherList {
    private WeatherMain main;
    private String dt_txt;
    private Weather[] weather;
    
    public WeatherList() {
       
    }

    public WeatherList(WeatherMain main) {
        this.main = main;
    }

    public WeatherMain getMain() {
        return main;
    }

    public void setMain(WeatherMain main) {
        this.main = main;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public Weather[] getWeather() {
        return weather;
    }
    
    
    
    
}
