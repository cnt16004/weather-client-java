/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.teiath.weatherclient.resources;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherForecast {

    private WeatherList[] list;
    private City city;

    public WeatherForecast() {
    }

    public WeatherForecast(WeatherList[] list) {
        this.list = list;
        //this.country = country; 
    }

    public WeatherList[] getList() {
        return list;
    }

    public void setList(WeatherList[] list) {
        this.list = list;
    }


    public City getCity() {
        return city;
    }
    

}
